import requests
from pprint import pprint
import os

class veriPhone:
    def __init__(self):
        os.system('clear')
        self.apiKey = '&key=A651B9A4009B455496127A543FD07529'
        veriPhone.getnumber(self)
        veriPhone.makeMenu(self)
    
    def makeMenu(self):
        print('Numer telefonu: '+ self.number + '\n')
        print('1. Potwierdź autentyczność numeru')
        print('2. Sprawdź nadawcę')
        print('3. Sprawdź typ numeru\n')
        self.menucounter = int(input(''))

        if self.menucounter == 1:
            veriPhone.isnumbervalid(self)
        elif self.menucounter == 2:
            veriPhone.checkcarrier(self)
        elif self.menucounter == 3:
            veriPhone.checktype(self)
        else:
            return veriPhone.makeMenu(self)

    def getnumber(self):
        print('Wpisz numer telefonu (razem z numerem kierunkowym)')
        self.number = str(input(''))
        self.phonenumber = 'phone=' + self.number
        os.system('clear')

    def isnumbervalid(self):
        #https://api.veriphone.io/v2/verify?phone=%2B49-15123577723&key=266B0091BC9547A2A40DD088795FA4C6
        
        self.base_url = "https://api.veriphone.io/v2/verify?phone=%2B" + self.phonenumber + self.apiKey
        self.request = requests.get(self.base_url).json()
        if self.request['phone_valid'] == True:
            print('Numer telefonu jest autentyczny')
        else:
            print('Numer telefonu nie jest autentyczny')

    def checkcarrier(self):
        self.base_url = "https://api.veriphone.io/v2/verify?phone=%2B" + self.phonenumber + self.apiKey
        self.request = requests.get(self.base_url).json()
        
        print('Nadawcą numeru jest ' + self.request['carrier'] + ' ' + self.request['country_code'])

    def checktype(self):
        self.base_url = "https://api.veriphone.io/v2/verify?phone=%2B" + self.phonenumber + self.apiKey
        self.request = requests.get(self.base_url).json()

        print(self.request['phone_type'])
veriPhone()